package cellular;
import cellular.CellState;
import cellular.GameOfLife;
import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    public BriansBrain(int i, int i1) {
        currentGeneration = new CellGrid(i, i1, CellState.DEAD);
        initializeCells();
    }
    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }
    }

    @Override
    public int numberOfRows() {
        // TODO
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        // TODO
        return currentGeneration.numColumns();
    }

    @Override
    public CellState getCellState(int row, int col) {
        // TODO
        return currentGeneration.get(row,col);
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();

        for (int i = 0; i < numberOfRows(); i++) {
            for (int j = 0; j < numberOfColumns(); j++) {
                nextGeneration.set(i,j,getNextCell(i,j));
            }
        }
        currentGeneration = nextGeneration;
        // TODO
    }

    @Override
    public CellState getNextCell(int row, int col) {
        CellState curr = getCellState(row, col);
        int neighbors = countNeighbors(row, col, CellState.ALIVE);

        if (curr == CellState.ALIVE) {
            return CellState.DYING;
        }
        if (curr == CellState.DYING) {
            return CellState.DEAD;
        }
        if (curr == CellState.DEAD && neighbors == 2) {
            return CellState.ALIVE;
        }
        else{
            return CellState.DEAD;
        }
    }


    /**
     * Calculates the number of neighbors having a given CellState of a cell on
     * position (row, col) on the board
     *
     * Note that a cell has 8 neighbors in total, of which any number between 0 and
     * 8 can be the given CellState. The exception are cells along the boarders of
     * the board: these cells have anywhere between 3 neighbors (in the case of a
     * corner-cell) and 5 neighbors in total.
     *
     * @param x     the x-position of the cell
     * @param y     the y-position of the cell
     * @param state the Cellstate we want to count occurences of.
     * @return the number of neighbors with given state
     */

    private int countNeighbors(int row, int col, CellState cs) {
        int count = 0;
        for (int i = row - 1; i <= row + 1; i++) {
            for (int j = col - 1; j <= col + 1; j++) {
                if (!(0 <= i && i <= this.numberOfRows() - 1 && 0 <= j && j <= this.numberOfColumns() - 1)) {
                    continue;
                }
                CellState state = getCellState(i,j);
                if (state.equals(cs) && (!(i == row && j == col))) {
                    count += 1;
                }
            }
        }
        return count;
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}